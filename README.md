# CloudNC Infrastructure Engineer Interview

This exercise takes a sample application that is now at the point where the application now needs to be developed by multiple engineers, tested and deployed.

## Allowing others to be able to work on the application

> It works on my laptop!

Given a list of requirements an infrastructure engineer will need to be able to provide easy steps for others to set their machine up so they can run the application locally. In this case the requirements are:

- Python
- Postgresql (or any other SQL database supported by SQLAlchemy)
- Elasticsearch
- An SMTP server for forgotten password functionality

and the following Python packages:

- flask
- flask-wtf
- flask-sqlalchemy
- flask-migrate
- psycopg2
- flask-login
- flask-mail
- pyjwt
- flask-bootstrap
- flask-moment
- flask-babel
- guess_language-spirit
- requests
- black
- flake8
- pylint
- coverage
- pytest
- elasticsearch
- python-dotenv

The following environment variables must also be set (the application also supports loading these from a `.env` file at the root of the repo):

- FLASK_APP=microblog.py
- FLASK_DEBUG=1 (for local development and testing, not for production)
- DATABASE_URL=postgresql://postgres:mysecretpassword@localhost:5432/postgres
- SECRET_KEY (optional but should be set outside of dev/test)
- ELASTICSEARCH_URL=localhost:9200 (optional)
- MS_TRANSLATOR_KEY (optional)
and the email config to allow the application to send forgotten password emails via SMTP:
- MAIL_SERVER
- MAIL_PORT
- MAIL_USE_TLS
- MAIL_USERNAME
- MAIL_PASSWORD

Run the application:
```sh
flask run
```

To create the schema/apply any migrations:

```sh
flask db upgrade
```

To create database migrations after any changes to models:
```sh
flask db migrate --message "Add foo to bar table"
```

To run the unit tests:
```sh
coverage run --source=app -m unittest tests.unit.test_unit
```

To see the unit test coverage report:
```sh
coverage report -m
```

To run a specific unit test:
```sh
python -m unittest tests.unit.test_unit.UserModelCase.test_avatar
```

To run the integration tests (needs non SQLite database and Elasticsearch):
```sh
pytest tests/integration
```

To auto format the code:
```sh
black app/ tests/ config.py microblog.py
```

To test for flake8 issues:
```sh
flake8 app/ tests/ config.py microblog.py --max-line-length=88
```

To test for pylint issues (note these are currently failing but we should track these and steadily improve):
```sh
pylint app/ tests/ config.py microblog.py
```

## Continuous integration

While having tests run locally is great, we also want to run them automatically when people push their changes so we can be sure that our changes don't break things because we forgot to run tests locally.

At CloudNC we use (self-hosted) Gitlab for our source control and also use Gitlab CI for continuous integration and deployment. Gitlab CI uses a [YAML file called `.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/) to describe the CI/CD pipeline.

Infrastructure engineers are normally involved in helping application engineers to design their CI pipeline and define it in the .gitlab-ci.yml file. With any necessary input from the application engineer you should build this CI pipeline.

You are also welcome to use other CI providers such as GitHub Actions, CircleCI, Jenkins etc if you are more comfortable/have more experience with those.

## Deployment

To be able to provide any actual benefit a change must be usable by customers.

In this part of the interview we will design how the application should be deployed to production. We will use [Excalidraw](https://excalidraw.com/) to whiteboard this remotely. You’re free to pick pretty much any technologies or services that you’d like during this stage.

You should show how the application is deployed and how it runs (including any relevant networking and security requirements) and how data is persisted. We’ll then discuss the options you chose so you should think about the benefits and trade offs of each of these things. We’ll also talk a little bit about what things need to be there on day 1 and what things can be done over time.

