from datetime import datetime, timedelta

import pytest

from app import create_app, db
from app.models import User, Post
from app.search import query_index
from config import Config


@pytest.fixture
def app():
    """Create and configure a new app instance for each test."""
    app = create_app(config_class=Config)

    app_context = app.app_context()
    app_context.push()

    yield app


def test_not_using_sqlite():
    assert "sqlite" not in Config.SQLALCHEMY_DATABASE_URI


def test_posts_end_up_in_elasticsearch(app):
    assert Config.ELASTICSEARCH_URL

    # create four users
    u1 = User(username="john", email="john@example.com")
    u2 = User(username="susan", email="susan@example.com")
    u3 = User(username="mary", email="mary@example.com")
    u4 = User(username="david", email="david@example.com")
    users = [u1, u2, u3, u4]
    db.session.add_all(users)

    # create four posts
    now = datetime.utcnow()
    p1 = Post(body="post from john", author=u1, timestamp=now + timedelta(seconds=1))
    p2 = Post(body="post from susan", author=u2, timestamp=now + timedelta(seconds=4))
    p3 = Post(body="post from mary", author=u3, timestamp=now + timedelta(seconds=3))
    p4 = Post(body="post from david", author=u4, timestamp=now + timedelta(seconds=2))
    db.session.add_all([p1, p2, p3, p4])
    db.session.commit()

    _, hits = query_index("post", "post from", 1, 2)

    # remove users
    for user in users:
        db.session.delete(user)
    db.session.commit()

    assert hits > 0
